include(CPackComponent)

include(packaging/cpackconfig_common.cmake)

cpack_add_component(vcredist DISPLAY_NAME "Install Visual Studio 2015-2022 Redistributable")
cpack_add_component(drivers DISPLAY_NAME "RAMBo Drivers" REQUIRED)

# ========================================
# NSIS
# ========================================
set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
set(CPACK_NSIS_EXECUTABLES_DIRECTORY ".")
set(CPACK_NSIS_STARTMENU_DIRECTORY "Cura LulzBot Edition")
set(CPACK_NSIS_INSTALLED_ICON_NAME "Cura.ico")
set(CPACK_NSIS_MUI_ICON ${CMAKE_SOURCE_DIR}\\\\packaging\\\\cura.ico)   # note: fails with forward '/'
set(CPACK_PACKAGE_ICON ${CMAKE_SOURCE_DIR}\\\\packaging\\\\cura.ico)

set(CPACK_NSIS_MENU_LINKS 
    "https://gitlab.com/lulzbot3d/cura-le" "Development Resources"
    "https://lulzbot.com/support/cura" "Cura LulzBot Edition Webpage"
)

set(CPACK_NSIS_MUI_HEADERIMAGE_BITMAP ${CMAKE_SOURCE_DIR}\\\\packaging\\\\cura-le-logo.bmp)
set(CPACK_NSIS_MUI_WELCOMEFINISHPAGE_BITMAP ${CMAKE_SOURCE_DIR}\\\\packaging\\\\cura-le-banner-nsis.bmp)    # note: fails with forward '/'
set(CPACK_NSIS_MUI_UNWELCOMEFINISHPAGE_BITMAP ${CMAKE_SOURCE_DIR}\\\\packaging\\\\cura-le-banner-nsis.bmp)
set(CPACK_NSIS_INSTALLER_MUI_FINISHPAGE_RUN_CODE "!define MUI_FINISHPAGE_RUN \\\"$WINDIR\\\\explorer.exe\\\"\n!define MUI_FINISHPAGE_RUN_PARAMETERS \\\"$INSTDIR\\\\CuraLE.exe\\\"")	# Hack to ensure Cura LE is not started with admin rights

#!/bin/sh
#
#Ensures the dependencies built with cura-build-environment can actually be found
cbe_install_dir="/home/curadev/CuraDev/environment"

export PATH="$cbe_install_dir/bin:$PATH"
export PKG_CONFIG_PATH="$cbe_install_dir/lib/pkgconfig:$PKG_CONFIG_PATH"
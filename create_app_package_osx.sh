#!/bin/sh

mkdir build
cd build

source ../env_osx.sh

cmake -DPython3_EXECUTABLE=/Users/lulzbotit/CuraDev/environment/bin/python3 ..

make